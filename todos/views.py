from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodosForm, TodosItemsForm

# Create your views here.


def show_todos(request):
    todos = TodoList.objects.all()
    context = {
        "todos_object": todos,
    }
    return render(request, "todos/list.html", context)


def todos_detail(request, id):
    detail = get_object_or_404(TodoList, id=id)
    context = {
        "detail_object": detail,
    }
    return render(request, "todos/detail.html", context)


def create_list(request):
    if request.method == "POST":
        form = TodosForm(request.POST)
        if form.is_valid():
            form = form.save()
            return redirect("todo_list_detail", id=form.id)
    else:
        form = TodosForm()

    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def edit_list(request, id):
    todos = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodosForm(request.POST, instance=todos)
        if form.is_valid():
            form = form.save()
            return redirect("todo_list_detail", id=form.id)
    else:
        form = TodosForm(instance=todos)
        context = {
            "detail_object": todos,
            "edit_form": form,
        }
        return render(request, "todos/edit.html", context)


def delete_list(request, id):
    todos = TodoList.objects.get(id=id)
    if request.method == "POST":
        todos.delete()
        return redirect("todo_list_list")
    else:
        context = {
            "detail_object": todos,
        }
        return render(request, "todos/delete.html", context)


def create_item(request):
    if request.method == "POST":
        form = TodosItemsForm(request.POST)
        if form.is_valid():
            form = form.save()
            return redirect("todo_list_detail", id=form.list.id)
    else:
        form = TodosItemsForm()

        context = {
            "items_form": form,
        }
        return render(request, "todos/item_create.html", context)


def update_item(request, id):
    todos = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodosItemsForm(request.POST, instance=todos)
        if form.is_valid():
            form = form.save()
            return redirect("todo_list_detail", id=form.list.id)
    else:
        form = TodosItemsForm(instance=todos)
        context = {
            "update_object": todos,
            "update_form": form,
        }
        return render(request, "todos/update.html", context)
